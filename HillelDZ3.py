
# 1. Зформуйте строку, яка містить певну інформацію про символ в відомому слові.
# Наприклад "The [номер символу] symbol in [тут слово] is '[символ з відповідним порядковим номером]'".
# Слово та номер отримайте за допомогою input() або скористайтеся константою. Наприклад (слово - "Python" а номер символу 3) - "The 3 symbol in "Python" is 't' ".

# word = input('Enter word ')
# word2 = ' ' + word
# index = int(input('Номер символу '))
# print(f'The {index} symbol in {word} is {word2[index]} ')


# 2. Написати цикл, який буде вимагати від користувача ввести слово, в якому є буква "о" (враховуються як великі так і маленькі).
# Цикл не повинен завершитися, якщо користувач ввів слово без букви о.

while True:
    word = input("Enter word ")
    sub1 = 'O' in word
    sub2 = 'o' in word
    if sub1 or sub2 is True:
        print(f'Ви ввели щось що містить о чи О або обидві - {word}')
        break
    else:
        print('Ви ввели щось без о чи О')






