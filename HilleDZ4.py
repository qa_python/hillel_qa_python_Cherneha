# Є list з даними lst1 = ['1', '2', 3, True, 'False', 5, '6', 7, 8, 'Python', 9, 0, 'Lorem Ipsum']. Напишіть код, який свормує новий list (наприклад lst2),
# який містить лише змінні типу стрінг, які присутні в lst1. Зауважте, що lst1 не є статичним і може формуватися динамічно.

lst1 = ['1', '2', 3, True, 'False', 5, '6', 7, 8, 'Python', 9, 0, 'Lorem Ipsum']
lst2 = []

for elem in lst1:
    if type(elem) == str:
        lst2.append(elem)
        continue
print(lst2)

# Є list довільних чисел, наприклад [11, 77, 4, 22, 0, 56, 5, 95, 7, 5, 87, 13, 45, 67, 44]. Напишіть код, який видалить з нього всі числа, які менше 21 і більше 74.

lst3 = [11, 77, 4, 22, 0, 56, 5, 95, 7, 5, 87, 13, 45, 67, 44]
lst4 = []
lst4.extend(lst3)

for elem in lst3:
    if elem >= 74:
        lst4.remove(elem)
        continue
    elif elem <= 21:
        lst4.remove(elem)
print(lst4)

# Є стрінг з певним текстом (можна скористатися input або константою). Напишіть код, який визначить кількість слів в цьому тексті, які закінчуються на 'о'.

text = "Learningo Python Iso Awesomeo"
splitted_text_list = text.split()

def condition(x):
    return x[-1] == "o"

number = sum(condition(x) for x in splitted_text_list)
print(number)



