import requests

# завдання 1 урл http://api.open-notify.org/astros.json вивести список всіх астронавтів, що перебувають наразі
# на орбіті (дані не фейкові, оновлюються в режимі реального часу)

url = 'http://api.open-notify.org/astros.json'

response = requests.get(url)
response_json = response.json()
people = response_json['people']

for name in people:
    print(name['name'])

# Завдання 2
# апі погоди (всі токени я для вас вже прописав)
# https://api.openweathermap.org/data/2.5/weather?q={city_name}&appid=47503e85fabbabc93cff28c52398ae97&units=metric
# де city_name - назва міста англійською мовою (наприклад, odesa, kyiv, lviv)
# погода змінюється, як і місто. яке ви введете
# видрукувати температуру та швидкість вітру. з вказівкою міста, яке було вибране


city_name = input('Введіть назву міста англійською мовою ')

url = f'https://api.openweathermap.org/data/2.5/weather?q={city_name}&appid=47503e85fabbabc93cff28c52398ae97&units=metric'

response = requests.get(url)
response_json = response.json()
town_name = response_json['name']
wind = response_json['wind']
main = response_json['main']
print('Wind Speed --->', wind['speed'])
print('Name of city --->', town_name)
print('Температура--->', main.get('temp'))



